/*funções para desenhas na tela*/
void normaliza_desenho(struct vet_vertices *vet_vertices, float *x_centro, float *y_centro, float *escala);
void desenha(SDL_Renderer* renderer, struct vet_vertices *vet_vertices, struct modelo *modelo);