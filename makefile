all: wireframe

clean:
	-rm *.out

purge:
	-rm wireframe

wireframe: wireframe.c perspect.c perspect.h graphics.c graphics.h objread.c objread.h datatypes.h
	gcc wireframe.c perspect.c graphics.c objread.c -Wall -lSDL2 -lm -o wireframe.out  