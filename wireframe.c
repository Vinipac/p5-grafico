<<<<<<< HEAD
#include <stdio.h>
#include <stdlib.h>
#include "datatypes.h"
#include "perspect.h"
#include "graphics.h"
#include "objread.h"


int main(int argc, char *argv[])
{
    /* variaveis de testes */
    char* arq_nome;
    struct camera cam;
    cam.x_c = cam.y_c = 3;
    cam.z_c = 200;    
    struct modelo modelo;    
    struct vet_vertices vet_vertices; //assim mesmo 
    

    if(argc == 2) {
        arq_nome = argv[1];
    }
    else 
        arq_nome = NULL;
    
    objread(&vet_vertices, arq_nome, &modelo);    
      


    /* inicialização do sdl */
    if(SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stderr, "ERRO AO INICIALIZER SDL\n");
        exit(1);
    }

    SDL_Window *window= NULL;
    SDL_Renderer *renderer = NULL;

    if(SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, 0 , &window, &renderer) != 0) {
        fprintf(stderr, "ERRO AO CRIAR JANELA!\n");
        exit(1);
    }

    /* tratando a movimentação */
    int fim = SDL_FALSE;
    SDL_Event ev;

    while(!fim)
    {
        while(SDL_PollEvent(&ev)) {
            if(ev.type == SDL_QUIT) {
                fim = SDL_TRUE;
            }
            else {
                if(ev.type == SDL_KEYDOWN) { //verifica se foi pressionado uma tecla;
                    switch(ev.key.keysym.sym)
                    {
                        case SDLK_UP:
                            cam.y_c-=5;
                            break;
                        case SDLK_DOWN:
                            cam.y_c+=5;
                            break;
                        case SDLK_LEFT:
                            cam.x_c-=5;
                            break;
                        case SDLK_RIGHT:
                            cam.x_c+=5;
                            break;
                        default:
                            break;
                        
                    }
                }
            }

        }
        /* AQUI IRÁ ACONTONCECER A CHAMADA DAS FUNÇÕES DE CALCULO */ 
        perspective(&modelo, cam); //calcula a perspectiva e atualiza no modelo       
        desenha(renderer, &vet_vertices, &modelo);        
    }  
     return 0;
}
