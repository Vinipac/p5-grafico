<<<<<<< HEAD
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#include "datatypes.h"

void objread (struct vet_vertices *vert_lista, char* arq_nome, struct modelo *modelo){
    FILE* modelo_arq;
    if(arq_nome == NULL) {
        modelo_arq = stdin;
    }
    else {
        modelo_arq = fopen(arq_nome, "r");
    }


    int num_vert = 0;
    int num_faces = 0;
    char* linha = NULL;
    size_t tam; 

    // Le quantidade de vertices e faces no arquivo


    while (getline(&linha, &tam, modelo_arq) != -1){      
    if ((linha[0] == 'v')&&(linha[1] == ' ')) num_vert++;
    if ((linha[0] == 'f')&&(linha[1] == ' ')) num_faces++;
    }

    // Aloca memoria

    vert_lista->vertices_totais = malloc(num_vert * sizeof(struct vertice));
    vert_lista->total_verts = num_vert;
    modelo->faces = malloc(num_faces * sizeof(struct face));
    modelo->total_faces = num_faces;

    rewind(modelo_arq);
    // Le o arquivo e coloca os vertices e faces nos seus lugares
    int vert_index,face_index;
    vert_index = face_index = 0;
    char* token;
    char del[2] = " ";
    char linha_aux[256];

    while (getline(&linha, &tam, modelo_arq) != -1) 
    {
        // Lendo um vertice
        if ((linha[0] == 'v') && (linha[1] == ' ')) {

            token = strtok(linha, del); // Token recebe 'v'

            // Token recebe o valor de x e o coloca no vetor de vertices        
            vert_lista->vertices_totais[vert_index].x = atof(strtok(NULL, del));
            // Token recebe o valor de y e o coloca no vetor de vertices
            vert_lista->vertices_totais[vert_index].y = atof(strtok(NULL, del));
            // Token recebe o valor de z e o coloca no vetor de vertices
            vert_lista->vertices_totais[vert_index].z = atof(strtok(NULL, del));

            vert_index++;
        }

        // Lendo uma face
        if (linha[0] == 'f') {
            int pontos_face = -1; int ponto_pos; int i=0;
            // pontos_face é -1 ja que a leitura do 'f' ira aumentar o valor da variavel em 1.

            // Le a quantidade de vertices na face
            strcpy(linha_aux, linha);
            token = strtok(linha_aux, del);
            while(token != NULL){     
                pontos_face++;
                token = strtok(NULL, del);
            }

            modelo->faces[face_index].vertices = malloc(pontos_face * sizeof(struct vertice));
            modelo->faces[face_index].num_vert = pontos_face;

            token = strtok(linha, del); // Recebe 'f'
            token = strtok(NULL, del);
                while (token != NULL){
                    ponto_pos = atoi(token); // Transforma a string token em um int
                    modelo->faces[face_index].vertices[i] = vert_lista->vertices_totais[ponto_pos-1];
                    token = strtok(NULL, del);
                    i++;
                }
            face_index++;
        }
    }  

}
