#include "datatypes.h"
#include <stdio.h>

void normaliza_desenho(struct vet_vertices *vet_vertices, float *x_centro, float *y_centro, float *escala) {
    float x_min, x_max, y_min, y_max, x_esc, y_esc, x_dif, y_dif;
    x_min = x_max = vet_vertices->vertices_totais[0].x;
    y_min = y_max = vet_vertices->vertices_totais[0].y;

    /*procura o min e max no vetor de vértices*/
    for(int i = 0; i < vet_vertices->total_verts; i++) {
        if(vet_vertices->vertices_totais[i].x < x_min)
            x_min = vet_vertices->vertices_totais[i].x;
        if(vet_vertices->vertices_totais[i].x > x_max)
            x_max = vet_vertices->vertices_totais[i].x;

        if(vet_vertices->vertices_totais[i].y < y_min)
            y_min = vet_vertices->vertices_totais[i].y;
        if(vet_vertices->vertices_totais[i].y > y_max)
            y_max = vet_vertices->vertices_totais[i].y;      
    }
    /*calcula o centro*/
    (*x_centro) = (x_max + x_min)/2;
    (*y_centro) = (y_max + y_min)/2;

    /*calcula a diferença*/
    x_dif = x_max - x_min;
    y_dif = y_max - y_min;

    /*calcula a escala*/
    x_esc = WIDTH/x_dif;
    y_esc = HEIGHT/y_dif;

    if (x_esc < y_esc)
        *escala = x_esc*0.7; //0.7 pra caber melhor na tela
    else
        *escala = y_esc*0.7; //0.7 pra caber melhor na tela  
    
}


void desenha(SDL_Renderer* renderer, struct vet_vertices *vet_vertices, struct modelo *modelo) {
    int i,j;
    float x_centro, y_centro, escala, x1_d, y1_d, x2_d, y2_d;
    escala = y_centro = x_centro = 0;
   
    normaliza_desenho(vet_vertices, &x_centro, &y_centro, &escala);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(renderer);
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);

    /*desenhas todas as faces menos a ultima linha*/ 

    for(i = 0; i < modelo->total_faces ; i ++) {  
        for(j = 0; j < modelo->faces[i].num_vert - 1; j++) {
            x1_d = (((modelo->faces[i].vertices[j].x_p) - x_centro) * escala) + (WIDTH/2);
            y1_d = (((modelo->faces[i].vertices[j].y_p) - y_centro) * escala) + (HEIGHT/2);
            x2_d = (((modelo->faces[i].vertices[j + 1].x_p) - x_centro) * escala) + (WIDTH/2);
            y2_d = (((modelo->faces[i].vertices[j + 1].y_p) - y_centro) * escala) + (HEIGHT/2);
            SDL_RenderDrawLine(renderer, x1_d, y1_d, x2_d, y2_d);

        }
          /* liga a ultima linha */
          x1_d = (((modelo->faces[i].vertices[modelo->faces[i].num_vert-1].x_p) - x_centro) * escala) + (WIDTH/2);
          y1_d = (((modelo->faces[i].vertices[modelo->faces[i].num_vert-1].y_p) - y_centro) * escala) + (HEIGHT/2);
          x2_d = (((modelo->faces[i].vertices[0].x_p) - x_centro) * escala) + (WIDTH/2);
          y2_d = (((modelo->faces[i].vertices[0].y_p) - y_centro) * escala) + (HEIGHT/2);
         SDL_RenderDrawLine(renderer, x1_d, y1_d, x2_d, y2_d);
    }
    SDL_RenderPresent(renderer);
}
