#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>
#include <allegro5/allegro.h>
#include "datatypes.h"
#define BUFFER_SIZE 120

FILE *read_obj_file(char *path)
{
    FILE *arq = fopen(path, "r") ;
    if (!arq)
    {
        perror("Erro ao abrir arquivo desejado!");
        exit(1); // encerra o programa com status 1
    }
    return arq;
}
int adiciona_elementos(OBJETO *obj,VERTICE *vertice, char *path)
{
    FILE *obj_file = read_obj_file(path);
    int i = 0;
    char *line_buffer = (char *)malloc(sizeof(char) * BUFFER_SIZE);
    char *line_tokens;
    /* Buffer para os vértices */
    float vertice_buffer[3];
    obj->faces = NULL;
    obj->cont = 0;
    vertice->cont = 0;
    while(!feof(obj_file))
    {
        fgets(line_buffer, BUFFER_SIZE, obj_file);
        /* Quebra a linha em tokens */
        line_tokens = strtok(line_buffer," ");
        /* Pega apenas os v e não vn, vo */
        if(line_tokens[0] == 'v' && line_tokens[1] == '\0')
        {
            /* Incrementa o número de vértices */
            vertice->cont++;
            i = 0;
            /* Saí do 'v' */
            line_tokens = strtok (NULL, " ");
            
            /* Quebra a linha para a leitura */
            while (line_tokens != NULL)
            {
                vertice_buffer[i] = atof(line_tokens);
                line_tokens = strtok (NULL, " ");
                i++;
            }
            vertice->x = realloc(vertice->x, vertice->cont * sizeof(float));
            vertice->y = realloc(vertice->y, vertice->cont * sizeof(float));
            vertice->z = realloc(vertice->z, vertice->cont * sizeof(float));
            vertice->x[vertice->cont-1] = vertice_buffer[0];
            vertice->y[vertice->cont-1] = vertice_buffer[1];
            vertice->z[vertice->cont-1] = vertice_buffer[2];
        }
        else if (line_tokens[0] == 'f')
        {
            /* Incremena a contagem de faces */
            obj->cont++;
            obj->faces = realloc(obj->faces, obj->cont * sizeof(FACE));
            /* Conta quantos vértices a face tem*/
            /* Pula o f */
            line_tokens = strtok(NULL, " ");
            /* Quebra a linha para a leitura */
            int i, vertice_atual = 0;
            FACE face;
            face.cont = 0;
            face.vertices = NULL;
            /* Separa o bloco da face entre os tokens */
            char *v_buffer = malloc(sizeof(char) * BUFFER_SIZE);
            while (line_tokens != NULL)
            {
                face.cont++;
                face.vertices = realloc(face.vertices, face.cont * sizeof(int)); 
                
                /* Faz o buffering no número da face antes da barra */
                i = 0;
                char c;
                c = line_tokens[i];
                while ((c != ' ') && (c != '/')) {
                    v_buffer[i] = c;
                    i++;
                    c = line_tokens[i];
                }
                /* Adiciona o terminador*/
                v_buffer[i] = '\0';
                vertice_atual++;
                face.vertices[vertice_atual-1] = atof(v_buffer);
                line_tokens = strtok(NULL, " ");
                i++;
            }
            free(v_buffer);
            obj->faces[(obj->cont)-1] = face;   
        }
    }
    
    fclose(obj_file);
    return 0;
}