#include "datatypes.h"
#include <stdio.h>

void perspect_vert(struct vertice *vert, struct camera cam) {  
    /*normaliza x e y para um dado vértice*/
    vert->x_p = cam.x_c + cam.z_c * (( (vert->x - cam.x_c) / (vert->z + cam.z_c) ));
    vert->y_p = cam.y_c + cam.z_c * (( (vert->y - cam.y_c) / (vert->z + cam.z_c) ));
}
void perspective(struct modelo *modelo, struct camera cam) {
    int i,j;
    /* para cada face do modelo, normaliza os vértices delas */
    for(i = 0; i < modelo->total_faces; i++) {
        for(j = 0; j < modelo->faces[i].num_vert;j++){        
            perspect_vert(&(modelo->faces[i].vertices[j]), cam);
        }
    }
}